using UnityEngine;
using UnityEngine.InputSystem;

public class TestCase : MonoBehaviour
{
    [SerializeField] private InputAction aKeyAction;
    [SerializeField] private InputAction escKeyAction;


    // Start is called before the first frame update
    void Start()
    {
        aKeyAction.Enable();
        escKeyAction.Enable();
    }


    // Update is called once per frame
    void Update()
    {
        if (aKeyAction.triggered)
        {
            Debug.Log("'A' key triggered, fullscreen toggled, no error expected");
            Screen.fullScreen = !Screen.fullScreen;
        }
        if (escKeyAction.triggered)
        {
            Debug.Log("'Esc' key triggered, fullscreen toggled, error expected");
            Screen.fullScreen = !Screen.fullScreen;
        }
    }
}
