# Reproduction for Unity WebGL fullscreen error

## What happened

When using an keyboard input action for *key Esc* to toggle the fullscreen mode, it leads to an error alert plus exceptions in log:

Failed to execute 'requestFullscreen' on 'Element': API can only be initiated by a user gesture.
requestFullscreen @ Builds.framework.js:10198
runDeferredCalls @ Builds.framework.js:9765
jsEventHandler @ Builds.framework.js:9790
Builds.loader.js:251 Invoking error handler due to
TypeError: fullscreen error
at Object.requestFullscreen (http://localhost:54979/Build/Builds.framework.js:10198:11)
    at Object.runDeferredCalls (http://localhost:54979/Build/Builds.framework.js:9765:24)
    at jsEventHandler (http://localhost:54979/Build/Builds.framework.js:9790:13)
Builds.framework.js:10198 Uncaught (in promise) TypeError: fullscreen error
    at Object.requestFullscreen (Builds.framework.js:10198:11)
    at Object.runDeferredCalls (Builds.framework.js:9765:24)
    at jsEventHandler (Builds.framework.js:9790:13)

## How can we reproduce it using the example you attached

"Build and Run" the project for WebGL. Run on Chrome (I used v98 Windows). Press Esc to toggle fullscreen. Error appears either on first or third time you press the button, depending on if you clicked in the window or not. It's appears to be due to Chrome not recognizing a user gesture. Key A also toggles fullscreen but it does not give an error.

[Screen recording (from Mac/Chrome)](Screen Recording 2022-03-01 at 11.30.58.mov)

If possible, this should be solved by ensuring Chrome always recognizes a gesture (as it is user triggered input). But if not, at least we don't want an unhandled exception that pops up to the user.

## Reproduced on

- Unity 2020.3.23f1 + Win Chrome v98
- Unity 2020.3.23f1 + Win Edge v98
- Unity 2020.3.21f1 + Mac Chrome v98
- Unity 2021.2.7f1 + Win Chrome v98